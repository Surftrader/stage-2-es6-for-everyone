import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

  firstFighter.initialHealth = firstFighter.health;
  secondFighter.initialHealth = secondFighter.health;

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    let deregisters = [];
    const checkWinner = function () {
      if (firstFighter.health <= 0 || secondFighter.health <= 0) {
        let winner = firstFighter.health <= 0 ? secondFighter : firstFighter;

        for (let someFunction of deregisters) {
          someFunction();
        }
        resolve(winner);
      }
    }

    let secondFighterBlocked = false;
    const secondFighterBlockStart = function () {
      secondFighterBlocked = true;
    }
    const secondFighterBlockEnd = function () {
      secondFighterBlocked = false;
    }

    deregisters.push(registerKeyHandler(controls.PlayerTwoBlock, secondFighterBlockStart, secondFighterBlockEnd));

    let firstFighterBlocked = false;
    const firstFighterBlockStart = function () {
      firstFighterBlocked = true;
    }
    const firstFighterBlockEnd = function () {
      firstFighterBlocked = false;
    }
    deregisters.push(registerKeyHandler(controls.PlayerOneBlock, firstFighterBlockStart, firstFighterBlockEnd));

    const firstFighterDamageHandler = function () {
      if (!secondFighterBlocked && !firstFighterBlocked) {
        secondFighter.health -= getDamage(firstFighter, secondFighter);
        updateHealthViews(firstFighter, secondFighter);
      } else if (secondFighterBlocked && !firstFighterBlocked) {
        updateHealthViews(firstFighter, secondFighter);
      }
      checkWinner();
    };
    deregisters.push(registerKeyHandler(controls.PlayerOneAttack, firstFighterDamageHandler));

    const secondFighterDamageHandler = function () {
      if (!firstFighterBlocked && !secondFighterBlocked) {
        firstFighter.health -= getDamage(secondFighter, firstFighter);
        updateHealthViews(firstFighter, secondFighter);
      } else if (firstFighterBlocked && !secondFighterBlocked) {
        updateHealthViews(firstFighter, secondFighter);
      }
      checkWinner();
    };
    deregisters.push(registerKeyHandler(controls.PlayerTwoAttack, secondFighterDamageHandler));

    const firstFighterCriticalDamage = function () {
      secondFighter.health -= getCriticalDamage(firstFighter);
      updateHealthViews(firstFighter, secondFighter);
      checkWinner();
    }
    deregisters.push(registerMultipleKeyHandler(...controls.PlayerOneCriticalHitCombination, firstFighterCriticalDamage));

    const secondFighterCriticalDamage = function () {
      firstFighter.health -= getCriticalDamage(secondFighter);
      updateHealthViews(firstFighter, secondFighter);
      checkWinner();

    }
    deregisters.push(registerMultipleKeyHandler(...controls.PlayerTwoCriticalHitCombination, secondFighterCriticalDamage));
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const blockPower = getBlockPower(defender);
  const hitPower = getHitPower(attacker);
  return blockPower > hitPower ? 0 : hitPower - blockPower;
}

export function getHitPower(fighter) {
  // return hit power
  const { attack } = fighter;
  const criticalHitChance = Math.random() + 1;
  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter;
  const dodgeChance = Math.random() + 1;
  return defense * dodgeChance;
}

export function getCriticalDamage(attacker) {
  const { attack } = attacker;
  return attack * 2;
}

function registerMultipleKeyHandler(key1, key2, key3, downCallback) {

  let criticalKey1 = false;
  let criticalKey2 = false;
  let criticalKey3 = false;
  let lastCallbackDate = null;

  function check() {
    if (criticalKey1 && criticalKey2 && criticalKey3) {
      const dateNow = Date.now();
      if (!lastCallbackDate || dateNow - lastCallbackDate > 10000) {
        lastCallbackDate = dateNow;
        downCallback();
      }
    }
  }

  const down1 = function () {
    criticalKey1 = true;
    check();
  }
  const down2 = function () {
    criticalKey2 = true;
    check();
  }
  const down3 = function () {
    criticalKey3 = true;
    check();
  }

  const up1 = function () {
    criticalKey1 = false;
  }
  const up2 = function () {
    criticalKey2 = false;
  }
  const up3 = function () {
    criticalKey3 = false;
  }

  const deregister1 = registerKeyHandler(key1, down1, up1);
  const deregister2 = registerKeyHandler(key2, down2, up2);
  const deregister3 = registerKeyHandler(key3, down3, up3);

  return () => {
    deregister1();
    deregister2();
    deregister3();
  }
}

function registerKeyHandler(key, downCallback, upCallback) {
  const keyDown = function (event) {
    if (downCallback && event.code == key && event.repeat === false) {
      downCallback();
    }
  };
  const keyUp = function (event) {
    if (upCallback && event.code == key) {
      upCallback();
    }
  };

  document.addEventListener('keydown', keyDown);
  document.addEventListener('keyup', keyUp);

  const deregister = () => {
    document.removeEventListener('keydown', keyDown);
    document.removeEventListener('keyup', keyUp);
  };

  return deregister;
}

function updateHealthViews(firstFighter, secondFighter) {

  const secondFighterIndicator = document.getElementById('right-fighter-indicator');
  const firstFighterIndicator = document.getElementById('left-fighter-indicator');

  const restSecondFighterHealth = secondFighter.health * 100 / secondFighter.initialHealth;
  secondFighterIndicator.style.background = `linear-gradient(90deg, #ebd759 ${restSecondFighterHealth}%, red 0)`;

  const restFirstFighterHealth = firstFighter.health * 100 / firstFighter.initialHealth;
  firstFighterIndicator.style.background = `linear-gradient(90deg, #ebd759 ${restFirstFighterHealth}%, red 0)`;
}
